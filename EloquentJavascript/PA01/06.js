function makeRabbit(type) {
    let rabbit = Object.create(protoRabbit);
    rabbit.type = type;
    return rabbit;
}

//function Rabbit(type) { /* constructor as funtion */
//    this.type = type;
//}

Rabbit.prototype.speak = function(line) {
    console.log(`the ${this.type} rabbit says ${line}`);
}

let weirdRabbit = new Rabbit("weird");

class Rabbit{
    constructor(type){
        this.type = type;
    }

    speak(line){
    console.log(`the ${this.type} rabbit says ${line}`);
    }
}
let killerRabbit = new Rabbit("killer");
let blackRabbit = new Rabbit("black");

let object = new class {getWord() {return "hello"; }};
console.log(object.getWord());

console.log(Array.prototype.toString == Object.prototype.toString);

const myObject = {
    city: 'Madrid',
    great(){
        console.log(`Greetings from ${this.city}`);
    }
}

console.log(myObject);
myObject.great();

const personPrototype = {
    great() {
        console.log(`hello, my name is ${this.name}!`);
    }
}

let okIterator = "OK"[Symbol.iterator](); 
console.log(okIterator.next());
console.log(okIterator.next());
console.log(okIterator.next());

/* ******************************************************* */



