class Temp {
    constructor(celsius) {
        this.celsius = celsius;
    }

    get fah() {
        return this.celsius * 1.8 + 32;
    }

    set fah(value) {
        this.celsius = (value - 32) / 1.8;
    }
}
