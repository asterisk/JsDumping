const roads = [
  "Alice's House-Bob's House",   "Alice's House-Cabin",
  "Alice's House-Post Office",   "Bob's House-Town Hall",
  "Daria's House-Ernie's House", "Daria's House-Town Hall",
  "Ernie's House-Grete's House", "Grete's House-Farm",
  "Grete's House-Shop",          "Marketplace-Farm",
  "Marketplace-Post Office",     "Marketplace-Shop",
  "Marketplace-Town Hall",       "Shop-Town Hall"
];

function buildGraph(edges) {
    let graph = Object.create(null);
    function addEdge(from, to) {
        if (graph[from] == null) {
            graph[from] = [to];
        } else {
            graph[from].push(to);
        }
    }
    for (let [from, to] of edges.map(r => r.split("-"))) {
        addEdge(from, to); 
        addEdge(to, from);
    }
    return graph;
}

const roadGraph = buildGraph(roads);

class VillageState {

    constructor(place, parcels) {
        this.place = place; /* string */
        this.parcels = parcels; /* array */
    }

    move(destination) {
        if (!roadGraph[this.place].includes(destination)) {
            return this;
        } else {
            let parcels = this.parcels.map(p => { 
                if (p.place != this.place) return p;
                return {place: destination, address: p.address};
            }).filter(p => p.place != p.address);
            return new VillageState(destination, parcels);
        }
    }
}

let first = new VillageState("Post Office",
    [{place: "Post Office", address: "Alice's House"}]
);

let next = first.move("Alice's House");

function runRobot(state, robot, memory) {
    for (let turn = 0; ; turn++) {
        if (state.parcels.length == 0 ) {
            console.log(`Done in ${turn} turns`);
            return turn;
        }
        let action = robot(state, memory); /* */
        state = state.move(action.direction); /* */
        memory = action.memory;
        // console.log(`Moved to ${action.direction}`);
    }
}

function randomPick(array) {
    let choice = Math.floor(Math.random() * array.length);
    return array[choice];
}

function randomRobot(state) { 
    return {direction: randomPick(roadGraph[state.place])};
}

VillageState.random = function(parcelCount = 5) {
    let parcels = [];
    for(let i = 0; i < parcelCount; i++) {
        let address = randomPick(Object.keys(roadGraph));
        let place;
        do {
            place = randomPick(Object.keys(roadGraph));
        } while (place == address);
        parcels.push({place, address});
    }
    return new VillageState("Post Office", parcels);
}

// runRobot(VillageState.random(), randomRobot);

function findRoute(graph, from, to) {
    let work = [{at: from, route: []}];
    for (let i = 0; i< work.length; i++) {
        let {at, route} = work[i]; 
        for (let place of graph[at]) {
            if (place == to) return route.concat(place);
            if (!work.some(w => w.at == place)) {
                work.push({at: place, route: route.concat(place)});
            }
        }
    }
}

function goalOrientedRobot({place, parcels}, route) {
    if (route.length == 0) {
        let parcel = parcels[0];
        if (parcel.place != place) {
            route = findRoute(roadGraph, place, parcel.place);
        } else {
            route = findRoute(roadGraph, place, parcel.address);
        }
    }
    return {direction: route[0], memory: route.slice(1)};
}

function compareRobots(robot1, memory1, robot2, memory2) {
    let sum1 = 0, sum2 = 0;
    for (let i = 0; i < 100; i++){
        sum1 += runRobot(VillageState.random(), robot1, memory1);
        sum2 += runRobot(VillageState.random(), robot2, memory2);
    }
    console.log("average the 1st robot: ", sum1/ 100, " average the second: ", sum2/100);
}

//compareRobots(randomRobot, [], goalOrientedRobot, []);

function optimizedRobot({place, parcels}, route) {
    let shortest_route = route;

    if(route.length == 0) {
        let routes = [];
        const PICK_UP = "Pick up";
        const DELIVER = "Delivery";

        for (let parcel of parcels) {
            if (parcel.place != place) {
                route = findRoute(roadGraph, place, parcel.place);
                routes.push({
                    path: route,
                    steps: route.length,
                    action_type: PICK_UP
                });
            }
            else {
                route = findRoute (roadGraph, place, parcel.address);
                routes.push({
                    path: route,
                    steps: route.length,
                    action_type: DELIVER
                });
            }
        }

        if (routes.some(route => route.action_type == PICK_UP)) {
            shortest_route = routes.filter(route => {
                return route.action_type == PICK_UP;
            }).reduce((minimum_route, route) => {
                return route.steps < minimum_route.steps ? route : minimum_route;
            }).path;
        }
        else {
            shortest_route = routes.reduce((minimum_route, route) => {
                return route.steps < minimum_route.steps ? route : minimum_route;
            }).path;
        }
    }

    return {
        direction:shortest_route[0],
        memory: shortest_route.slice(1)
    };
}

compareRobots(goalOrientedRobot, [], optimizedRobot, []);
