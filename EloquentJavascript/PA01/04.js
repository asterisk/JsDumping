let journal = [];

function addEntry(events, squirrel) {
    journal.push({events, squirrel});
}

addEntry ("huh", 4)

let arr = [1, 2, 3, 2, 1];

let i = 6;

while( i-- > 0 ) {
    console.log(arr.slice(0, i));
    console.log(arr.slice(1, i));
}

function max(...numbers) { /* takes any number of arguemnts: not limited */
    let res = -Infinity;

    for(let n of numbers) {
        if (n > res) res = n;
    }
    return res;
}

function gT(n) {
    return m => m > n;
}

let gT10 = gT(10);
console.log(gT10(1));

function noisy(f){
    return (...args) => {
        console.log("calling with", args);
        let result = f(...args); 
        console.log(", returned ", result);
        return result;
    };
}

["A", "B"].forEach((n) => console.log(n));

function filter(array, test) {
    let passed = [];
    for (let element of array) {
        if (test(element)) {
            passed.push(element);
        }
    }
    return passed;
}

