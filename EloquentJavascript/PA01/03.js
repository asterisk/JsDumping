function multi(factor) {
    return (number) => number * factor; /* arrow function */
}

function isEven(l) { /* isEven  */
    if (l == 0)
        return "even";
    if (l == 1)
        return "odd";
    return isEven(l - 2);
}

console.log(isEven(5));

